<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable = [
        'user_id',
        'judul',
        'isi',
        'gambar',
        'ttd',
        'tanggal',
        'status'
    ];
}
