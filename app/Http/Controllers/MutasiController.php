<?php

namespace App\Http\Controllers;

use App\Tempat;
use App\User;
use App\Pegawai;
use App\DetailPegawai;
use App\Mutasi;
use Illuminate\Http\Request;

class MutasiController extends Controller
{
    public function mutasi(){
        $no = 1;
        $mutasis = Mutasi::all();
        return view('mutasi.mutasi', compact('no','mutasis'));
    }

    public function tempat(){
        $no = 1;
        $tempats = Tempat::all();
        return view('mutasi.sub-bag', compact('no','tempats'));
    }

    public function allPegawai($id){
        $no = 1;
        $tempat = Tempat::where('id',$id)->first();
        $pegawais = Pegawai::where('tempat_id', $id)->get();
        $tempats = Tempat::where('sisa','>',0)
                    ->where('id', '!=', $id)
                    ->get();
        return view('mutasi.index', compact('tempat','no','pegawais', 'tempats'));
    }

    public function mutasiPegawai(Request $r){
        $pgw = Pegawai::where('user_id', $r->user_id)->first();
        $pegawai = Pegawai::where('user_id',$r->user_id)->update([
            'tempat_id' => $r->tempat_id
        ]);
        
        $tempat_lama = Tempat::where('id',$r->tempat_lama)->first();
        $jml_lama = $tempat_lama->jumlah - 1;
        $sisa_lama = $tempat_lama->sisa + 1;
        $tempat_lm = Tempat::where('id', $tempat_lama->id)->update([
            'jumlah' => $jml_lama,
            'sisa' => $sisa_lama
        ]);
        $tempat_baru = Tempat::where('id',$r->tempat_id)->first();
        $jml_baru = $tempat_baru->jumlah + 1;
        $sisa_baru = $tempat_baru->sisa - 1;

        // dd($sisa_baru);

        $tempat = Tempat::where('id',$r->tempat_id)->update([
            'jumlah' => $jml_baru,
            'sisa' => $sisa_baru
        ]);
        // dd($r->user_id);
        $mutasi = Mutasi::create([
            'pegawai_id' => $pgw->id,
            'tempat_lama' => $tempat_lama->id,
            'tempat_baru' => $r->tempat_id,
            'keterangan' => $r->keterangan
        ]);

        toastr()->success('Pegawai berhasil di mutasi!');
        return redirect('mutasi/'.$tempat_baru->id.'/pegawai');
    }
}
