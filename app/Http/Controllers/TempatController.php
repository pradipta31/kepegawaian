<?php

namespace App\Http\Controllers;

use Validator;
use App\Tempat;
use Illuminate\Http\Request;

class TempatController extends Controller
{
    public function index(){
        $no = 1;
        $tempats = Tempat::all();
        return view('tempat.index', compact('no', 'tempats'));
    }

    public function create(){
        return view('tempat.create');
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'nama_tempat' => 'required|unique:tempats',
            'slot' => 'required|numeric'
        ]);
        if (!$validator->fails()) {
            $tempat = Tempat::create([
                'nama_tempat' => $r->nama_tempat,
                'slot' => $r->slot,
                'jumlah' => 0,
                'sisa' => $r->slot
            ]);
            toastr()->success('Sub Bagian baru berhasil ditambahkan!');
            return redirect('tempat');
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function update(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'nama_tempat' => 'required',
            'slot' => 'required|numeric'
        ]);
        if (!$validator->fails()) {
            $tempat = Tempat::where('id',$id)->update([
                'nama_tempat' => $r->nama_tempat,
                'slot' => $r->slot,
                'sisa' => $r->slot
            ]);
            toastr()->success('Sub Bagian berhasil di ubah!');
            return redirect('tempat');
        }else{
            toastr()->error($validator->messages()->first());
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id){
        $tempat = Tempat::where('id',$id)->delete();
        toastr()->success('Sub bagian yang dipilih berhasil dihapus!');
        return redirect('tempat');
    }
}
