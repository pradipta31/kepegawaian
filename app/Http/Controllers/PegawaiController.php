<?php

namespace App\Http\Controllers;

use Validator;
use App\Tempat;
use App\User;
use App\Pegawai;
use App\DetailPegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    public function tempat(){
        $no = 1;
        $tempats = Tempat::all();
        return view('sub-bag', compact('no','tempats'));
    }

    public function allPegawai($id){
        $no = 1;
        $tempat = Tempat::where('id',$id)->first();
        $pegawais = Pegawai::where('tempat_id', $id)->get();
        return view('pegawai.index', compact('tempat','no','pegawais'));
    }

    public function editGolkat($id){
        $pegawai = Pegawai::where('user_id', $id)->first();
        return view('pegawai.edit', compact('pegawai'));
    }

    public function updateGolkat(Request $r){
        $validator = Validator::make($r->all(), [
            'golongan' => 'required',
            'pangkat' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            //dd($r->all());
            $pegawai = Pegawai::where('user_id', $r->user_id)->update([
                'pangkat' => $r->pangkat,
                'golongan' => $r->golongan
            ]);

            toastr()->success('Golongan/pangkat pegawai berhasil diubah!');
            return redirect('pegawai/'.$r->tempat_id.'/datapegawai');
        }
    }

    public function dataPegawai(){
        $no = 1;
        $users = Pegawai::all();
        return view('pegawai.datapegawai', compact('no','users'));
    }
}
