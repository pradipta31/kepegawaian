<?php

namespace App\Http\Controllers;

use Image;
use Validator;
use Auth;
use App\Announcement;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{
    public function index(){
        $no = 1;
        $pengumumans = Announcement::all();
        return view('pengumuman.index', compact('no','pengumumans'));
    }

    public function create(){
        return view('pengumuman.create');
    }

    public function store(Request $r){
        $v = Validator::make($r->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'ttd' => 'required',
            'tanggal' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('gambar')) {
                $this->validate($r, [
                    'gambar' => 'required',
                    'gambar.*' => 'image|mimes:jpeg,png,jpg|max:5024'
                ]);
                $gambar = $r->file('gambar');
                $filename = time() . '.' . $gambar->getClientOriginalExtension();
                Image::make($gambar)->save(public_path('/images/pengumuman/'.$filename));
                $pengumuman = Announcement::create([
                    'judul' => $r->judul,
                    'isi' => $r->isi,
                    'gambar' => $filename,
                    'ttd' => $r->ttd,
                    'tanggal' => $r->tanggal,
                    'user_id' => Auth::id(),
                    'status' => $r->status
                ]);
                toastr()->success('Pengumuman baru berhasil ditampilkan ke seluruh pegawai!');
                return redirect('pengumuman');
            }else{
                $pengumuman = Announcement::create([
                    'judul' => $r->judul,
                    'isi' => $r->isi,
                    'ttd' => $r->ttd,
                    'user_id' => Auth::id(),
                    'tanggal' => $r->tanggal,
                    'status' => $r->status
                ]);
                toastr()->success('Pengumuman baru berhasil ditampilkan ke seluruh pegawai!');
                return redirect('pengumuman');
            }
        }
    }

    public function edit($id){
        $pengumuman = Announcement::where('id',$id)->first();
        return view('pengumuman.edit', compact('pengumuman'));
    }

    public function update(Request $r, $id){
        $v = Validator::make($r->all(), [
            'judul' => 'required',
            'isi' => 'required',
            'ttd' => 'required',
            'tanggal' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('gambar')) {
                $this->validate($r, [
                    'gambar' => 'required',
                    'gambar.*' => 'image|mimes:jpeg,png,jpg|max:5024'
                ]);
                $gambar = $r->file('gambar');
                $filename = time() . '.' . $gambar->getClientOriginalExtension();
                Image::make($gambar)->save(public_path('/images/pengumuman/'.$filename));
                $pengumuman = Announcement::where('id',$id)->update([
                    'judul' => $r->judul,
                    'isi' => $r->isi,
                    'gambar' => $filename,
                    'ttd' => $r->ttd,
                    'tanggal' => $r->tanggal,
                    'status' => $r->status
                ]);
                toastr()->success('Pengumuman berhasil diubah!');
                return redirect('pengumuman');
            }else{
                $pengumuman = Announcement::where('id',$id)->update([
                    'judul' => $r->judul,
                    'isi' => $r->isi,
                    'ttd' => $r->ttd,
                    'tanggal' => $r->tanggal,
                    'status' => $r->status
                ]);
                toastr()->success('Pengumuman berhasil diubah!');
                return redirect('pengumuman');
            }
        }
    }
}
