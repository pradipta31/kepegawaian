<?php

namespace App\Http\Controllers;

use App\Tempat;
use App\User;
use App\Announcement;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pengumumans = Announcement::where('status',1)->get();
        $countPane = [
            'pegawai' => User::where('level', 3)->count(),
            'tempat' => Tempat::all()->count(),
            'pengumuman' => Announcement::all()->count(),
        ];
        return view('dashboard', compact('pengumumans'), $countPane);
    }
}
