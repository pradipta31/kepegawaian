<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use Image;
use Auth;
use App\User;
use App\Pegawai;
use App\DetailPegawai;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        if (Auth::user()->id == 1) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('profile-admin', compact('user'));
        } else {
            $pegawai = Pegawai::where('user_id',Auth::user()->id)->first();
            $user = User::where('id', $pegawai->user_id)->first();
            $detail = DetailPegawai::where('pegawai_id', $pegawai->id)->first();
            return view('profile', compact('pegawai', 'user', 'detail'));
        }
        
    }

    public function updateProfile(Request $r, $id){
        $validator = Validator::make($r->all(),[
            'username' => 'required',
            'nama' => 'required',
            'email' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $profile = User::where('id', $id)->update([
                'username' => $r->username,
                'nama' => $r->nama,
                'email' => $r->email,
            ]); 

            $pegawai = DetailPegawai::where('pegawai_id', $r->pegawai_id)->update([
                'jenis_kelamin' => $r->jenis_kelamin,
                'tempat_lahir' => $r->tempat_lahir,
                'tanggal_lahir' => $r->tanggal_lahir,
                'agama' => $r->agama,
                'alamat' => $r->alamat,
                'no_hp' => $r->no_hp
            ]);
            toastSuccess('Profile berhasil diubah!');
            return redirect()->back();
        }
    }

    public function updateAva(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'avatar' => 'required|mimes:jpeg,png,jpg|max:5000'
        ]);
        //dd($r->all());
        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $user = User::where('id',$id)->first();

            $avatar = $r->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            
            if ($user->avatar != null) {
                unlink(public_path('/images/ava/').$user->avatar);
            }

            Image::make($avatar)->save(public_path('/images/ava/'.$filename));
            
            $user->update([
                'avatar' => $filename
            ]);

            toastSuccess('Update foto profil berhasil');
            return redirect()->back();
        }
    }

    public function updatePassword(Request $r, $id){
        $user = User::where('id',$id)->update([
            'password' => Hash::make($r->password)
        ]);
        toastSuccess('Password berhasil diubah!');
        return redirect()->back();
    }
}
