<?php

namespace App\Http\Controllers;

use Auth;
use App\Absen;
use App\Pegawai;
use App\User;
use App\DetailPegawai;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;

class AbsenController extends Controller
{
    public function index(){
        $no = 1;
        $pegawais = Pegawai::all();
        return view('absen.pegawai', compact('no','pegawais'));
    }

    public function absenPegawai($id){
        $no = 1;
        $pegawai = Pegawai::where('id',$id)->first();
        $absens = Absen::where('pegawai_id', $id)->get();
        return view('absen.index', compact('no', 'absens', 'pegawai'));
    }

    public function attendance(){
        return view('attendance');
    }

    public function absenMe(){
        $no = 1;
        $pegawai = Pegawai::where('user_id',Auth::user()->id)->first();
        $absens = Absen::where('pegawai_id', $pegawai->id)->get();
        return view('absen.index', compact('no', 'absens', 'pegawai'));
    }

    public function absen(Request $r){
        $date = date("Y-m-d");
        $nip = $r->nip;
        $user = User::where('nip', $nip)->first();
        $pegawai = Pegawai::where('user_id', $user->id)->first();
        $checkAbsen = Absen::where('pegawai_id',$pegawai->id)
                            ->where('tanggal',$date)
                            ->first();
        // dd($checkAbsen->id);
        
        // $hours_now = date('H');
        // if (date('H') < '07') {
        //     echo "Jam ".$hours_now;
        // }else{
        //     echo "Jam lewat";
        // }
        if ($checkAbsen == null) {
            $absen = Absen::create([
                'pegawai_id' => $pegawai->id,
                'tanggal' => $date,
                'jam_datang' => date("Y-m-d H:i:s")
            ]);

            toastSuccess('Absensi kedatangan berhasil! NIP: '.$user->nip. ', Nama: '.$user->nama);
            return redirect()->back();
        }else{
            if ($checkAbsen->jam_pulang == null) {
                if (date('H') < '16') {
                    toastError('sudah absen kedatangan hari ini! NIP: '.$user->nip. ', Nama: '.$user->nama);
                    return redirect()->back();
                } else {
                    $absen = Absen::where('id',$checkAbsen->id)->update([
                        'jam_pulang' => date("Y-m-d H:i:s")
                    ]);
        
                    toastSuccess('Absensi pulang berhasil! NIP: '.$user->nip. ', Nama: '.$user->nama);
                    return redirect()->back();
                }
                
            }else{
                toastError('Anda sudah absen hari ini! NIP: '.$user->nip. ', Nama: '.$user->nama);
                return redirect()->back();
            }
        }
        
    }

    public function cetakAbsen($pegawai_id){
        $spreadsheet = new Spreadsheet();
        $pegawai = Pegawai::where('user_id', $pegawai_id)->first();
        $absents = Absen::where('pegawai_id',$pegawai->id)->get();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B1', 'Pegawai : '.$pegawai->user->nama);
        $sheet->setCellValue('A3', 'No');
        $sheet->setCellValue('B3', 'Tanggal');
        $sheet->setCellValue('C3', 'Jam Datang');
        $sheet->setCellValue('D3', 'Jam Pulang');

        $row = 4;
        $row_pegawai = 1;
        $nomor = 1;
        foreach($absents as $absen){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,date('d-m-Y', strtotime($absen->tanggal)));
            $sheet->setCellValue('C'.$row,date('h:i:s', strtotime($absen->jam_datang)));
            $sheet->setCellValue('D'.$row,date('h:i:s', strtotime($absen->jam_pulang)));
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('AbsenPegawai'.$pegawai->user->nip.'.xlsx');
        return response()->download(public_path('AbsenPegawai'.$pegawai->user->nip.'.xlsx'))->deleteFileAfterSend();
    }

    public function cariAbsen($id){
        $pegawai = Pegawai::where('user_id', $id)->first();
        return view('absen.absensearch', compact('pegawai'));
    }

    public function searchAbsen(Request $r){
        $no = 1;
        $pegawai = Pegawai::where('id',$r->pegawai_id)->first();
        $absens = [];
        $tanggal_awal = $r->tanggal_awal;
        $tanggal_akhir = $r->tanggal_akhir;
        $absens = Absen::whereBetween('tanggal',[$r->tanggal_awal,$r->tanggal_akhir])
                  ->where('pegawai_id', $pegawai->id)
                  ->get();
        //dd($pegawai->id);
        return view('absen.detail-absen')->with([
            'no' => $no,
            'pegawai' => $pegawai,
            'absens' => $absens,
            'tanggal_awal' => $tanggal_awal,
            'tanggal_akhir' => $tanggal_akhir
        ]);
    }

    public function cetakkkk($tanggal_awal, $tanggal_akhir, $pegawai_id){
        $spreadsheet = new Spreadsheet();
        $pegawai = Pegawai::where('id', $pegawai_id)->first();
        $absents = Absen::whereBetween('tanggal',[$tanggal_awal,$tanggal_akhir])
                    ->where('pegawai_id', $pegawai->id)
                    ->get();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B1', 'Pegawai : '.$pegawai->user->nama);
        $sheet->setCellValue('A3', 'No');
        $sheet->setCellValue('B3', 'Tanggal');
        $sheet->setCellValue('C3', 'Jam Datang');
        $sheet->setCellValue('D3', 'Jam Pulang');

        $row = 4;
        $row_pegawai = 1;
        $nomor = 1;
        foreach($absents as $absen){
            $sheet->setCellValue('A'.$row,$nomor++);
            $sheet->setCellValue('B'.$row,date('d-m-Y', strtotime($absen->tanggal)));
            $sheet->setCellValue('C'.$row,date('h:i:s', strtotime($absen->jam_datang)));
            $sheet->setCellValue('D'.$row,date('h:i:s', strtotime($absen->jam_pulang)));
            $row++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('AbsenPegawai'.$pegawai->user->nip.'.xlsx');
        return response()->download(public_path('AbsenPegawai'.$pegawai->user->nip.'.xlsx'))->deleteFileAfterSend();
    }
}
