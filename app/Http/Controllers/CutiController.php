<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Cuti;
use App\Tempat;
use App\Pegawai;
use Illuminate\Http\Request;

class CutiController extends Controller
{

    public function index(){
        $no = 1;
        $user = Pegawai::where('user_id', Auth::user()->id)->first();
        $cutis = Cuti::where('pegawai_id',$user->id)->get();
        return view('cuti.datacuti', compact('no','cutis'));
    }

    public function cuti(){
        $no = 1;
        $cutis = Cuti::all();
        return view('cuti.index', compact('no','cutis'));
    }

    public function allPegawai(){
        $no = 1;
        $pegawais = Pegawai::all();
        return view('cuti.pegawai', compact('no', 'pegawais'));
    }

    public function tambahCuti($id){
        $pegawai = Pegawai::where('user_id', $id)->first();
        return view('cuti.create', compact('pegawai'));
    }

    public function addCuti(){
        $pegawai = Pegawai::where('user_id', Auth::user()->id)->first();
        return view('cuti.tambah', compact('pegawai'));
    }

    public function store(Request $r){
        $validator = Validator::make($r->all(), [
            'lama_cuti' => 'required|numeric',
            'tanggal_cuti' => 'required',
            'akhir_cuti' => 'required',
            'keterangan' => 'required',
            'file' => 'required|mimes:pdf|max:5000'
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            $pegawai = Pegawai::where('user_id', $r->user_id)->first();

            $document = $r->file('file');
            $file = time().'.'.$document->getClientOriginalExtension();
            $path_url = 'file/cuti';
            $document->storeAs($path_url,$file,'public');

            $cuti = Cuti::create([
                'pegawai_id' => $pegawai->id,
                'lama_cuti' => $r->lama_cuti,
                'tanggal_cuti' => date('Y-m-d', strtotime($r->tanggal_cuti)),
                'akhir_cuti' => date('Y-m-d', strtotime($r->akhir_cuti)),
                'keterangan' => $r->keterangan,
                'file' => $file,
                'status' => 2
            ]);
            toastSuccess('Cuti baru berhasil ditambahkan!');
            return redirect('pegawai/cuti');
        }
    }

    public function download($id){
        return response()->file(base_path('storage/app/public/file/cuti/'.$id));
    }

    public function editCuti($id){
        $cuti = Cuti::where('id',$id)->first();
        $pegawai = Pegawai::where('id',$cuti->pegawai_id)->first();
        return view('cuti.edit', compact('cuti','pegawai'));
    }

    public function updateCuti(Request $r, $id){
        $validator = Validator::make($r->all(), [
            'lama_cuti' => 'required|numeric',
            'tanggal_cuti' => 'required',
            'akhir_cuti' => 'required',
            'keterangan' => 'required',
        ]);

        if ($validator->fails()) {
            toastError($validator->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->hasFile('file') != NULL) {
                $val = Validator::make($r->all(), [
                    'file' => 'required|mimes:pdf|max:5000'
                ]);
                if ($val->fails()) {
                    toastError($val->messages()->first());
                    return redirect()->back()->withInput();
                } else {
                    $document = $r->file('file');
                    $file = time().'.'.$document->getClientOriginalExtension();
                    $path_url = 'file/cuti';
                    $document->storeAs($path_url,$file,'public');
    
                    $cuti = Cuti::where('id',$id)->update([
                        'lama_cuti' => $r->lama_cuti,
                        'tanggal_cuti' => date('Y-m-d', strtotime($r->tanggal_cuti)),
                        'akhir_cuti' => date('Y-m-d', strtotime($r->akhir_cuti)),
                        'keterangan' => $r->keterangan,
                        'file' => $file
                    ]);
                    toastSuccess('Data cuti berhasil di ubah!');
                    return redirect('datacuti');
                }
                
            } else {
                $cuti = Cuti::where('id',$id)->update([
                    'lama_cuti' => $r->lama_cuti,
                    'tanggal_cuti' => date('Y-m-d', strtotime($r->tanggal_cuti)),
                    'akhir_cuti' => date('Y-m-d', strtotime($r->akhir_cuti)),
                    'keterangan' => $r->keterangan
                ]);
                toastSuccess('Data cuti berhasil di ubah!');
                return redirect('datacuti');
            }
            
        }
    }

    public function setujuCuti(Request $r, $id){
        $cuti = Cuti::where('id',$id)->update([
            'user_id' => Auth::user()->id,
            'status' => 1
        ]);

        toastSuccess('Data cuti berhasil disetujui!');
        return redirect('datacuti');
    }

    public function tolakCuti(Request $r, $id){
        $cuti = Cuti::where('id',$id)->update([
            'user_id' => Auth::user()->id,
            'status' => 0
        ]);

        toastSuccess('Data cuti telah ditolak!');
        return redirect('datacuti');
    }
}
