<?php

namespace App\Http\Controllers;

use Hash;
use PDF;
use QrCode;
use Validator;
use App\Tempat;
use App\User;
use App\Pegawai;
use App\DetailPegawai;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $no = 1;
        $users = Pegawai::all();
        return view('user.index', compact('no','users'));
    }

    public function qrCode($id){
        $user = User::where('id',$id)->first();
        $qrcode = base64_encode(QrCode::format('svg')->size(200)->errorCorrection('H')->generate($user->nip));
        $pdf = PDF::loadView('pdf_view', ['qrcode' => $qrcode]);
        return $pdf->stream();
    }

    public function create(){
        $tempats = Tempat::where('sisa','>',0)->get();
        return view('user.create', compact('tempats'));
    }

    public function store(Request $r){
        $v = Validator::make($r->all(), [
            'tempat_id' => 'required',
            'nip' => 'required|unique:users|numeric',
            'nama' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
            'level' => 'required',
            'golongan' => 'required',
            'pangkat' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            
            // dd($r->all());
            $tempat = Tempat::where('id',$r->tempat_id)->first();
            $jumlah = $tempat->jumlah + 1;
            $sisa = $tempat->sisa - 1;
            // dd($sisa);
            $users = User::create([
                'nip' => $r->nip,
                'nama' => $r->nama,
                'username' => $r->username,
                'email' => $r->email,
                'password' => Hash::make($r->password),
                'level' => $r->level,
                'status' => 1
            ]);

            $pegawai = Pegawai::create([
                'user_id' => $users->id,
                'tempat_id' => $r->tempat_id,
                'pangkat' => $r->pangkat,
                'golongan' => $r->golongan
            ]);

            $detail = DetailPegawai::create([
                'pegawai_id' => $pegawai->id
            ]);
            
            $tempat = Tempat::where('id',$r->tempat_id)->update([
                'jumlah' => $jumlah,
                'sisa' => $sisa
            ]);

            toastr()->success('Pegawai baru berhasil ditambahkan!');
            return redirect(url('user'));
        }
    }

    public function edit($id){
        $tempats = Tempat::all();
        $user = Pegawai::where('user_id',$id)->first();
        return view('user.edit', compact('user', 'tempats'));
    }

    public function update(Request $r, $id){
        $v = Validator::make($r->all(), [
            'nip' => 'required',
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'level' => 'required',
            'status' => 'required'
        ]);

        if ($v->fails()) {
            toastError($v->messages()->first());
            return redirect()->back()->withInput();
        }else{
            if ($r->password == null) {
                $user = User::where('id',$id)->update([
                    'nip' => $r->nip,
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'level' => $r->level,
                    'status' => $r->status
                ]);
            }else{
                $user = User::where('id',$id)->update([
                    'nip' => $r->nip,
                    'nama' => $r->nama,
                    'username' => $r->username,
                    'email' => $r->email,
                    'password' => Hash::make($r->password),
                    'level' => $r->level,
                    'status' => $r->status
                ]);
            }

            toastr()->success('Data pegawai berhasil diubah!');
            return redirect(url('user'));
        }
    }
}
