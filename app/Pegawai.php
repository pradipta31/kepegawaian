<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $fillable = [
        'user_id',
        'tempat_id',
        'qr_pegawai',
        'pangkat',
        'golongan'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function tempat(){
        return $this->belongsTo('App\Tempat');
    }
}
