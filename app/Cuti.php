<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $fillable = [
        'pegawai_id',
        'lama_cuti',
        'tanggal_cuti',
        'akhir_cuti',
        'keterangan',
        'file',
        'status'
    ];

    public function pegawai(){
        return $this->belongsTo('App\Pegawai');
    }
}
