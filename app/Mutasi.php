<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mutasi extends Model
{
    protected $fillable = ['pegawai_id','tempat_lama','tempat_baru','keterangan'];

    public function pegawai(){
        return $this->belongsTo('App\Pegawai');
    }
}
