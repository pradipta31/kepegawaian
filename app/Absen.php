<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    protected $fillable = [
        'pegawai_id', 'tanggal', 'jam_datang', 'jam_pulang'
    ];
}
