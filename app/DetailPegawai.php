<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPegawai extends Model
{
    protected $fillable = [
        'pegawai_id',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'agama',
        'alamat',
        'no_hp'
    ];
}
