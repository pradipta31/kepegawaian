<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FROM PRADIPTA To ASRI PRADNYANI</title>
</head>
<body>
    <style>
        @import url(https://fonts.googleapis.com/css?family=Montez);@import url(https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:100,400,700,800);.container{width:100%}.column{padding:.5rem 1rem;font-size:inherit}@media (min-width:50rem){.container{width:80%;margin-left:10%;margin-right:10%}.column{float:left}.clearfix:after,.clearfix:before{content:" ";display:table}.clearfix:after{clear:both}.clearfix{*zoom:1}*,:after,:before{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box}body,html{height:100%;width:100%}a{text-decoration:none;color:inherit}html{font-size:100%}.fullpage{height:100%;width:100%;text-align:center;position:relative;-webkit-background-size:100% 100%;background-size:100% 100%}.quote-block{color:#fff;padding-top:25%;font-size:200%;text-shadow:0 1px 20px rgba(0,0,0,.4)}.quote-block span{display:block;line-height:.85em}.quote-block span.a,.quote-block span.c{font-family:'Alegreya Sans SC',sans-serif}.quote-block span.b,.quote-block span.d{font-family:Montez,cursive;letter-spacing:.04em}#section1{background:transparent 0 0 no-repeat fixed url(http://langmode.pl/wp-content/uploads/2013/08/blurred-background-10-2000x12501.jpg)}#section2{background:transparent 0 0 no-repeat fixed url(http://pic3-1.yourswallpaper.com/computer/bokeh/blurred_sunset-wallpaper-1366x768.jpg)}#section3{background:transparent 0 0 no-repeat fixed url(http://www.wallpaperup.com/uploads/wallpapers/2015/04/01/651835/18288e226d44aab4796aa83e5149d31b.jpg)}#section4{background:transparent 0 0 no-repeat fixed url(https://wallpaperscraft.com/image/flower_glare_blurred_bloom_spring_29818_1920x1080.jpg)}#section5{background:transparent 0 0 no-repeat fixed url(http://www.zastavki.com/pictures/originals/2013/Nature___Flowers___Pink_rose_on_a_blurred_background_048630_.jpg)})}
    </style>
    {{-- <div class="fullpage" id="section1">
        <div class="container">
            <div class="clearfix">
                <div class="quote-block">
                    <span class="a">A happy relationship isn't always easy,</span>
                    <span class="b">It takes a long time to get this far,</span>
                    <span class="c">to understand eachother and</span>
                    <span class="d">to know how much you mean to your love.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="fullpage" id="section2">
        <div class="container">
            <div class="clearfix">
                <div class="quote-block">
                    <span class="a">Life with you</span>
                    <span class="b">is more beautiful than</span>
                    <span class="c">any sunset or situation</span>
                    <span class="d">I've ever seen.</span>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="fullpage" id="section3">
        <div class="container">
            <div class="clearfix">
                <div class="quote-block">
                    <span class="a">Whatever happens in the future,</span>
                    <span class="b">as long as you want me,</span>
                    <span class="c">I'll be there and will</span>
                    <span class="d">love you more than my life.</span>
                </div>
            </div>
        </div>
    </div>
    {{-- <div class="fullpage" id="section4">
        <div class="container">
            <div class="clearfix">
                <div class="quote-block">
                    <span class="a">Times may get darker,</span>
                    <span class="b">but we'll make them brighter,</span>
                    <span class="c">brighter than we can ever imagine.</span>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="fullpage" id="section5">
        <div class="container">
            <div class="clearfix">
                <div class="quote-block">
                    <span class="a">Roses are red,</span>
                    <span class="b">Violets are blue,</span>
                    <span class="d">I LOVE YOU</span>
                    <span class="d">Asri Pradnyani</span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>