<li class="header">MASTER NAVIGATION</li>
<li class="{{$activeMenu == 'tempat' ? 'active' : ''}}"><a href="{{url('tempat')}}"><i class="fa fa-building-o"></i> <span>Data Tempat</span></a></li>
<li class="treeview {{$activeMenu == 'user' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Pegawai</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('user/tambah')}}"><i class="fa fa-plus"></i> Tambah Pegawai Baru</a></li>
        <li><a href="{{url('user')}}"><i class="fa fa-circle-o"></i> Data Pegawai</a></li>
    </ul>
</li>
{{-- <li class="header">PEGAWAI NAVIGATION</li> --}}

{{-- <li class="{{$activeMenu == 'pegawai' ? 'active' : ''}}"><a href="{{url('pegawai')}}"><i class="fa fa-group"></i> <span>Data Pegawai</span></a></li> --}}
{{-- <li class="{{$activeMenu == 'absen' ? 'active' : ''}}"><a href="{{url('absen')}}"><i class="fa fa-list-ul"></i> <span>Data Absen</span></a></li>
<li class="{{$activeMenu == 'mutasi' ? 'active' : ''}}"><a href="{{url('datamutasi')}}"><i class="fa fa-exchange"></i> <span>Data Mutasi Pegawai</span></a></li>
<li class="{{$activeMenu == 'cuti' ? 'active' : ''}}"><a href="{{url('datacuti')}}"><i class="fa fa-calendar"></i> <span>Data Cuti</span></a></li>

<li class="header">ANNOUNCEMENT NAVIGATION</li>
<li class="treeview {{$activeMenu == 'pengumuman' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-sticky-note-o"></i>
        <span>Pengumuman</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('pengumuman/tambah')}}"><i class="fa fa-plus"></i> Tambah Pengumuman Baru</a></li>
        <li><a href="{{url('pengumuman')}}"><i class="fa fa-circle-o"></i> Data Pengumuman</a></li>
    </ul>
</li> --}}