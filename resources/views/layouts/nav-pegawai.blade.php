<li class="header">PEGAWAI NAVIGATION</li>

<li class="{{$activeMenu == 'pegawai' ? 'active' : ''}}"><a href="{{url('pegawai/all')}}"><i class="fa fa-group"></i> <span>Data Pegawai</span></a></li>
<li class="{{$activeMenu == 'absen' ? 'active' : ''}}"><a href="{{url('absenme')}}"><i class="fa fa-list-ul"></i> <span>Data Absen</span></a></li>
<li class="{{$activeMenu == 'mutasi' ? 'active' : ''}}"><a href="{{url('datamutasi')}}"><i class="fa fa-exchange"></i> <span>Data Mutasi Pegawai</span></a></li>
{{-- <li class="{{$activeMenu == 'cuti' ? 'active' : ''}}"><a href="{{url('pegawai/cuti')}}"><i class="fa fa-calendar"></i> <span>Data Cuti Pegawai</span></a></li> --}}
<li class="treeview {{$activeMenu == 'cuti' ? 'active' : ''}}">
    <a href="#">
        <i class="fa fa-calendar"></i>
        <span>Cuti Pegawai</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{url('cuti/tambah')}}"><i class="fa fa-plus"></i> Permohonan Cuti</a></li>
        <li><a href="{{url('pegawai/cuti')}}"><i class="fa fa-circle-o"></i> History Cuti</a></li>
    </ul>
</li>