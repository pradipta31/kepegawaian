@extends('layouts.master',['activeMenu' => 'user'])
@section('title','Edit Pegawai')
@section('breadcrumb', 'Edit Pegawai')
@section('detail_breadcrumb', 'Edit Pegawai Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('user/'.$user->user_id.'/edit')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="length" value="6">
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Pegawai {{$user->user->nama}}</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Sub Bagian</label>
                                <select name="tempat_id" class="form-control" value="{{$user->tempat_id}}" disabled>
                                    <option value="0">Pilih Sub Bagian</option>
                                    @foreach ($tempats as $tempat)
                                        <option value="{{$tempat->id}}" {{$user->tempat_id == $tempat->id ? 'selected' : ''}}>{{$tempat->nama_tempat}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">NIP</label>
                                <input type="text" name="nip" class="form-control" value="{{$user->user->nip}}" placeholder="Masukan NIP">
                                <small>NIP ini akan digunakan sebagai QR Code.</small>
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{$user->user->nama}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" value="{{$user->user->username}}" placeholder="Masukkan Username">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{$user->user->email}}" placeholder="Masukkan Email">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                        <small>Kosongkan jika tidak ingin mengubah password.</small>
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Pilih Level User</label>
                                <select name="level" class="form-control" value="{{$user->user->level}}">
                                    <option value="" >Pilih Level User</option>
                                    <option value="2" {{$user->user->level == 2? 'selected' : ''}}>Admin</option>
                                    <option value="3" {{$user->user->level == 3? 'selected' : ''}}>Pegawai</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Pilih Golongan</label>
                                <select name="golongan" class="form-control" value="{{$user->golongan}}" disabled>
                                    <option value="">Pilih Golongan</option>
                                    <option value="1" {{$user->golongan == 1 ? 'selected' : ''}}>GOLONGAN I (Juru)</option>
                                    <option value="2" {{$user->golongan == 2 ? 'selected' : ''}}>GOLONGAN II (Pengatur)</option>
                                    <option value="3" {{$user->golongan == 3 ? 'selected' : ''}}>GOLONGAN III (Penata)</option>
                                    <option value="4" {{$user->golongan == 4 ? 'selected' : ''}}>GOLONGAN IV (Pembina)</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Pilih Pangkat</label>
                                <select name="pangkat" class="form-control" value="{{$user->pangkat}}" disabled>
                                    <option value="Juru Tingkat I" {{$user->pangkat == 'Juru Tingkat I' ? 'selected' : ''}}>Juru Tingkat I</option>
                                    <option value="Juru" {{$user->pangkat == 'Juru' ? 'selected' : ''}}>Juru</option>
                                    <option value="Juru Muda Tingkat I" {{$user->pangkat == 'Juru Muda Tingkat I' ? 'selected' : ''}}>Juru Muda Tingkat I</option>
                                    <option value="Juru Muda" {{$user->pangkat == 'Juru Muda' ? 'selected' : ''}}>Juru Muda</option>

                                    <option value="Pengatur Tingkat I" {{$user->pangkat == 'Pengatur Tingkat I' ? 'selected' : ''}}>Pengatur Tingkat I</option>
                                    <option value="Pengatur" {{$user->pangkat == 'Pengatur' ? 'selected' : ''}}>Pengatur</option>
                                    <option value="Pengatur Muda Tingkat I" {{$user->pangkat == 'Pengatur Muda Tingkat I' ? 'selected' : ''}}>Pengatur Muda Tingkat I</option>
                                    <option value="Pengatur Muda" {{$user->pangkat == 'Pengatur Muda' ? 'selected' : ''}}>Pengatur Muda</option>

                                    <option value="Penata Tingkat I" {{$user->pangkat == 'Penata Tingkat I' ? 'selected' : ''}}>Penata Tingkat I</option>
                                    <option value="Penata" {{$user->pangkat == 'Penata' ? 'selected' : ''}}>Penata</option>
                                    <option value="Penata Muda Tingkat I" {{$user->pangkat == 'Penata Muda Tingkat I' ? 'selected' : ''}}>Penata Muda Tingkat I</option>
                                    <option value="Penata Muda" {{$user->pangkat == 'Penata Muda' ? 'selected' : ''}}>Penata Muda</option>

                                    <option value="Pembina Utama" {{$user->pangkat == 'Pembina Utama' ? 'selected' : ''}}>Pembina Utama</option>
                                    <option value="Pembina UtamaMady" {{$user->pangkat == 'Pembina UtamaMady' ? 'selected' : ''}}>Pembina UtamaMady</option>
                                    <option value="Pembina Utama Muda" {{$user->pangkat == 'Pembina Utama Muda' ? 'selected' : ''}}>Pembina Utama Muda</option>
                                    <option value="Pembina Tingkat I" {{$user->pangkat == 'Pembina Tingkat I' ? 'selected' : ''}}>Pembina Tingkat I</option>
                                    <option value="Pembina" {{$user->pangkat == 'Pembina' ? 'selected' : ''}}>Pembina</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Pilih Status</label>
                                <select name="status" class="form-control" value="{{$user->user->status}}">
                                    <option value="1" {{$user->user->status == 1 ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{$user->user->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                </select>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }

    </script>
@endsection