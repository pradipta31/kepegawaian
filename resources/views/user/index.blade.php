@extends('layouts.master',['activeMenu' => 'user'])
@section('title','Data Pegawai')
@section('breadcrumb', 'Data Pegawai')
@section('detail_breadcrumb', 'Manajemen Data Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('user/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Pegawai
                        </a>
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Bagian</th>
                                        <th>NIP</th>
                                        <th>QR Code</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Level</th>
                                        <th>Golongan/Pangkat</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->tempat->nama_tempat}}</td>
                                            <td>
                                                {{$user->user->nip}}
                                            </td>
                                            <td>
                                                <a href="{{url('user/qrcode/'.$user->user_id)}}" target="_blank" class="btn btn-primary btn-sm">
                                                    Cetak QRCode
                                                </a>
                                            </td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                @if ($user->user->level == 2)
                                                    <span class="label label-success">Admin</span>
                                                @else
                                                    <span class="label label-success">Pegawai</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->golongan == 1)
                                                    GOLONGAN I (Juru)/{{$user->pangkat}}
                                                @elseif($user->golongan == 2)
                                                    GOLONGAN II (Pengatur)/{{$user->pangkat}}
                                                @elseif($user->golongan == 3)
                                                    GOLONGAN III (Penata)/{{$user->pangkat}}
                                                @elseif($user->golongan == 4)
                                                    GOLONGAN IV (Pembina)/{{$user->pangkat}}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->user->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('user/'.$user->user_id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
