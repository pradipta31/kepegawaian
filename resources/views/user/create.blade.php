@extends('layouts.master',['activeMenu' => 'user'])
@section('title','Tambah Pegawai')
@section('breadcrumb', 'Tambah Pegawai')
@section('detail_breadcrumb', 'Tambah Pegawai Baru')
@section('content')
    @include('layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('user/tambah')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="length" value="6">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Pegawai Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Sub Bagian</label>
                                <select name="tempat_id" class="form-control" value="{{old('tempat_id')}}">
                                    <option value="0">Pilih Sub Bagian</option>
                                    @foreach ($tempats as $tempat)
                                        <option value="{{$tempat->id}}">{{$tempat->nama_tempat}} - Sisa {{$tempat->sisa}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">NIP</label>
                                <input type="text" name="nip" class="form-control" value="{{old('nip')}}" placeholder="Masukan NIP">
                                <small>NIP ini akan digunakan sebagai QR Code.</small>
                            </div>
                            <div class="form-group">
                                <label for="">Nama</label>
                                <input type="text" name="nama" class="form-control" value="{{old('nama')}}" placeholder="Masukan Nama">
                            </div>
                            <div class="form-group">
                                <label for="">Username</label>
                                <input type="text" name="username" class="form-control" value="{{old('username')}}" placeholder="Masukkan Username">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="email" name="email" class="form-control" value="{{old('email')}}" placeholder="Masukkan Email">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-9">
                                        <label class="col-form-label">Password</label>
                                        <input type="text" class="form-control" name="password" value="{{old('password')}}">
                                    </div>
                                    <div class="col-md-3">
                                        <label class="col-form-label"></label>
                                        <button type="button" class="btn btn-success btn-md" onclick="generate();" style="margin-top: 20%">
                                            <i class="fa fa-refresh"></i>
                                            Generate
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @if (Auth::user()->level == 1)
                                <div class="form-group">
                                    <label for="">Pilih Level User</label>
                                    <select name="level" class="form-control" value="{{old('level')}}">
                                        <option value="0">Pilih Level User</option>
                                        <option value="2">Admin</option>
                                        <option value="3">Pegawai</option>
                                    </select>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="">Pilih Level User</label>
                                    <select name="level" class="form-control" value="{{old('level')}}">
                                        <option value="0">Pilih Level User</option>
                                        {{-- <option value="2">Admin</option> --}}
                                        <option value="3">Pegawai</option>
                                    </select>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="">Pilih Golongan</label>
                                <select name="golongan" class="form-control" value="{{old('golongan')}}" id="golongan_id">
                                    <option value="">Pilih Golongan</option>
                                    <option value="1">GOLONGAN I (Juru)</option>
                                    <option value="2">GOLONGAN II (Pengatur)</option>
                                    <option value="3">GOLONGAN III (Penata)</option>
                                    <option value="4">GOLONGAN IV (Pembina)</option>
                                </select>
                            </div>

                            @include('user.pangkat')
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function randomPassword(length) {
            var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        }

        function generate() {
            formUser.password.value = randomPassword(formUser.length.value);
        }

        $('#pangkat_1').hide();
        $('#pangkat_2').hide();
        $('#pangkat_3').hide();
        $('#pangkat_4').hide();

        $('#golongan_id').on('change',function(){
            var valueSelected = this.value;
            
            if(valueSelected == 1){
                $('#pangkat_1').show();
                $('#golongan_1').show();
                $('#golongan_1').prop('disabled', false);
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');

                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();
            }else if(valueSelected == 2){
                $('#pangkat_1').hide();
                $('#pangkat_2').show();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();

                $('#golongan_2').show();
                $('#golongan_2').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }else if(valueSelected == 3){
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').show();
                $('#pangkat_4').hide();
                
                $('#golongan_3').show();
                $('#golongan_3').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }else if(valueSelected == 4){
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').show();
                
                $('#golongan_4').show();
                $('#golongan_4').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
            }else{
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }
        });

        function saveThis(r){
            swal({
                title: "User baru akan ditambahkan",
                text: "User akan menerima email dan harus melakukan konfirmasi untuk dapat melakukan login.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((result) => {
                if (result) {
                    swal("Email berhasil terkirim!", {
                        icon: "success",
                    });
                }
            });
        }
    </script>
@endsection