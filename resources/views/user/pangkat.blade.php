<div class="form-group" id="pangkat_1">
    <label for="">Pilih Pangkat</label>
    <select name="pangkat" id="golongan_1" class="form-control" value="{{old('pangkat')}}">
        <option value="Juru Tingkat I">Juru Tingkat I</option>
        <option value="Juru">Juru</option>
        <option value="Juru Muda Tingkat I">Juru Muda Tingkat I</option>
        <option value="Juru Muda">Juru Muda</option>
    </select>
</div>

<div class="form-group" id="pangkat_2">
    <label for="">Pilih Pangkat</label>
    <select name="pangkat" id="golongan_2" class="form-control" value="{{old('pangkat')}}">
        <option value="Pengatur Tingkat I">Pengatur Tingkat I</option>
        <option value="Pengatur">Pengatur</option>
        <option value="Pengatur Muda Tingkat I">Pengatur Muda Tingkat I</option>
        <option value="Pengatur Muda">Pengatur Muda</option>
    </select>
</div>

<div class="form-group" id="pangkat_3">
    <label for="">Pilih Pangkat</label>
    <select name="pangkat" id="golongan_3" class="form-control" value="{{old('pangkat')}}">
        <option value="Penata Tingkat I">Penata Tingkat I</option>
        <option value="Penata">Penata</option>
        <option value="Penata Muda Tingkat I">Penata Muda Tingkat I</option>
        <option value="Penata Muda">Penata Muda</option>
    </select>
</div>

<div class="form-group" id="pangkat_4">
    <label for="">Pilih Pangkat</label>
    <select name="pangkat" id="golongan_4" class="form-control" value="{{old('pangkat')}}">
        <option value="Pembina Utama">Pembina Utama</option>
        <option value="Pembina UtamaMady">Pembina UtamaMady</option>
        <option value="Pembina Utama Muda">Pembina Utama Muda</option>
        <option value="Pembina Tingkat I">Pembina Tingkat I</option>
        <option value="Pembina">Pembina</option>
    </select>
</div>