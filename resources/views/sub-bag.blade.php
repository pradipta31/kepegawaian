@extends('layouts.master',['activeMenu' => 'pegawai'])
@section('title','Data Golongan dan Pangkat')
@section('breadcrumb', 'Data Golongan dan Pangkat')
@section('detail_breadcrumb', 'Data Data Golongan dan Pangkat')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tabelTempat" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Tempat</th>
                                        <th>Deskripsi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tempats as $tempat)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$tempat->nama_tempat}}</td>
                                            <td>
                                                @if ($tempat->deskripsi == null)
                                                    -
                                                @else
                                                    {{$tempat->deskripsi}}
                                                @endif
                                            </td>
                                            <td>
                                                @if (str_contains(url()->current(), '/mutasi'))
                                                    <a href="{{url('mutasi/'.$tempat->id.'/pegawai')}}" class="btn btn-primary btn-md">
                                                        <i class="fa fa-arrow-right"></i>
                                                        Data Pegawai
                                                    </a>
                                                @elseif(str_contains(url()->current(), '/pegawai'))
                                                    <a href="{{url('pegawai/'.$tempat->id.'/datapegawai')}}" class="btn btn-primary btn-md">
                                                        <i class="fa fa-arrow-right"></i>
                                                        Data Pegawai
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelTempat').dataTable()
        });
    </script>
@endsection
