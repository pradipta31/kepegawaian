@extends('layouts.master',['activeMenu' => 'cuti'])
@section('title','Permohonan Cuti')
@section('breadcrumb', 'Permohonan Cuti')
@section('detail_breadcrumb', 'Data Permohonan Cuti')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        {{-- <a href="{{url('cuti')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-calendar"></i>
                            Permohonan Cuti
                        </a> --}}
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Lama Cuti</th>
                                        <th>Tanggal Cuti</th>
                                        <th>Akhir Cuti</th>
                                        <th>Keterangan</th>
                                        <th>File</th>
                                        <th style="width: 100px">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cutis as $cuti)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$cuti->pegawai->user->nip}}
                                            </td>
                                            <td>{{$cuti->pegawai->user->nama}}</td>
                                            <td>
                                                {{$cuti->lama_cuti}} Hari
                                            </td>
                                            <td>
                                                {{$cuti->tanggal_cuti}}
                                            </td>
                                            <td>{{$cuti->akhir_cuti}}</td>
                                            <td>
                                                {{$cuti->keterangan}}
                                            </td>
                                            <td>
                                                <a href="{{ route('cuti.download',$cuti->file) }}" class="btn btn-primary"><i class="fa fa-print"></i> Download File</a>
                                            </td>
                                            <td>
                                                @if ($cuti->status == 2)
                                                    <a href="javascript:void(0);" class="btn btn-primary btn-sm" onclick="setujuCuti('{{$cuti->id}}')">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="tolakCuti('{{$cuti->id}}')">
                                                        <i class="fa fa-ban"></i>
                                                    </a>
                                                @else
                                                    @if ($cuti->status == 1)
                                                        <span class="label label-success">Disetujui</span>
                                                    @elseif($cuti->status == 0)
                                                        <span class="label label-danger">Ditolak</span>
                                                    @endif
                                                @endif
                                                {{-- <a href="{{url('datacuti/'.$cuti->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form class="hidden" action="" method="post" id="formSetuju">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
    <form class="hidden" action="" method="post" id="formTolak">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
    </form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function setujuCuti(id){
            swal({
                title: "Anda yakin?",
                text: "Cuti pegawai akan disetujui!",
                icon: "success",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Berhasil! Cuti pegawai berhasil disetujui!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formSetuju').attr('action', '{{url('pegawai/cuti/setuju')}}/'+id);
                        $('#formSetuju').submit();
                    }); 
                }
            });
        }

        function tolakCuti(id){
            swal({
                title: "Anda yakin?",
                text: "Cuti pegawai akan ditolak!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willActive) => {
                if (willActive) {
                    swal("Cuti pegawai telah ditolak!", {
                        icon: "warning",
                    }).then((res) => {
                        $('#formTolak').attr('action', '{{url('pegawai/cuti/tolak')}}/'+id);
                        $('#formTolak').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
