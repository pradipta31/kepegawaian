@extends('layouts.master',['activeMenu' => 'cuti'])
@section('title','Edit Permohonan Cuti Pegawai')
@section('breadcrumb', 'Edit Permohonan Cuti Pegawai')
@section('detail_breadcrumb', 'Edit Permohonan Cuti Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('datacuti/'.$cuti->id.'/edit')}}" name="formUser" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4>NIP : <b>{{$pegawai->user->nip}}</b></h4>
                            <h4>Nama : <b>{{$pegawai->user->nama}}</b></h4>
                            <h4>
                                Golongan/Pangkat : 
                                @if ($pegawai->golongan == 1)
                                    <b>GOLONGAN I (Juru)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 2)
                                    <b>GOLONGAN II (Pengatur)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 3)
                                    <b>GOLONGAN III (Penata)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 4)
                                    <b>GOLONGAN IV (Pembina)/{{$pegawai->pangkat}}</b>
                                @endif
                            </h4>
                            <h4>Bagian : <b>{{$pegawai->tempat->nama_tempat}}</b></h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Lama Cuti (hari)</label>
                                <input type="text" name="lama_cuti" class="form-control" value="{{$cuti->lama_cuti}}" placeholder="Masukan Lama cuti">
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Cuti</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="tanggal_cuti" value="{{$cuti->tanggal_cuti}}" class="form-control pull-right" id="datepicker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Akhir Cuti</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="akhir_cuti" value="{{$cuti->akhir_cuti}}" class="form-control pull-right" id="datepicker2">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <textarea name="keterangan" cols="30" rows="5" class="form-control">{{$cuti->keterangan}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Scan Cuti (PDF)</label>
                                <input type="file" class="form-control" name="file">
                                <small>Kosongkan jika tidak ingin mengubah file cuti.</small>
                            </div>
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script src="{{asset('backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('#datepicker2').datepicker({
            autoclose: true
        })
    </script>
@endsection