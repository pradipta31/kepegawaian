@extends('layouts.master',['activeMenu' => 'cuti'])
@section('title','Cuti')
@section('breadcrumb', 'Cuti')
@section('detail_breadcrumb', 'Data Cuti')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (Auth::user()->level == 2)
                            <h3>
                                Pegawai: {{Auth::user()->nama}}
                            </h3>
                        @else
                            <a href="{{url('cuti/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                                <i class="fa fa-plus"></i>
                                Tambah Permohonan Cuti
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Lama Cuti</th>
                                        <th>Tanggal Cuti</th>
                                        <th>Akhir Cuti</th>
                                        <th>Keterangan</th>
                                        <th>File</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($cutis as $cuti)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$cuti->lama_cuti}} Hari
                                            </td>
                                            <td>
                                                {{$cuti->tanggal_cuti}}
                                            </td>
                                            <td>{{$cuti->akhir_cuti}}</td>
                                            <td>
                                                {{$cuti->keterangan}}
                                            </td>
                                            <td>
                                                <a href="{{ route('cuti.download',$cuti->file) }}" class="btn btn-primary"><i class="fa fa-print"></i> Download File</a>
                                            </td>
                                            <td>
                                                @if ($cuti->status == 1)
                                                    <span class="label label-success">Disetujui</span>
                                                @elseif($cuti->status == 0)
                                                    <span class="label label-danger">Ditolak</span>
                                                @elseif($cuti->status == 2)
                                                    <span class="label label-warning">Dalam Proses</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });
    </script>
@endsection
