@extends('layouts.master',['activeMenu' => 'dashboard'])
@section('title','Dashboard')
@section('breadcrumb', 'Dashboard')
@section('detail_breadcrumb', 'Control Panel')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{$pegawai}}</h3>
        
                        <p>Jumlah Pegawai</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-purple">
                    <div class="inner">
                        <h3>{{$tempat}}</h3>
        
                        <p>Jumlah Sub Bagian</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-building"></i>
                    </div>
                
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$pengumuman}}</h3>
        
                        <p>Jumlah Pengumuman</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-sticky-note"></i>
                    </div>
                
                </div>
            </div>
            <div class="col-lg-12 col-xs-12 col-md-12">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-check"></i> Selamat datang {{Auth::user()->nama}}</h4>
                    Pada halaman dashboard anda dapat melihat beberapa informasi mengenai website anda.
                </div>

                

                <ul class="timeline">

                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-red">
                            PENGUMUMAN
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                
                    <!-- timeline item -->
                    @foreach ($pengumumans as $pengumuman)
                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-bell bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-clock-o"></i> {{date('d M, Y H:i:s', strtotime($pengumuman->created_at))}}</span>
                    
                                <h2 class="timeline-header"><a href="#">{{$pengumuman->judul}}</a></h2>
                    
                                <div class="timeline-body">
                                    {!! $pengumuman->isi !!}
                                    <div class="img-responsive">
                                        <img src="{{asset('images/pengumuman/'.$pengumuman->gambar)}}" alt="" class="img-responsive">
                                    </div>
                                </div>
                    
                                <div class="timeline-footer">
                                    <h5>TTD
                                        <br>
                                        {{$pengumuman->ttd}}
                                    </h5>
                                </div>
                            </div>
                        </li>
                    @endforeach
                
                </ul>
            </div>
        </div>
    </section>
@endsection

@section('js')
    
@endsection
