@extends('layouts.master',['activeMenu' => 'absen'])
@section('title','Absensi')
@section('breadcrumb', 'Absensi')
@section('detail_breadcrumb', 'Data Absensi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                    @if (Auth::user()->level == 3)
                        
                    @else
                        <a href="{{url('absen/cari/'.$pegawai->user_id)}}" class="btn btn-primary btn-md pull-right" style="margin-right: 10px; margin-top: 10px">
                            <i class="fa fa-search"></i>
                            Cari Absen
                        </a>
                        <a href="{{url('absen/cetak/'.$pegawai->user_id)}}" class="btn btn-success btn-md pull-right" style="margin-right: 10px; margin-top: 10px">
                            <i class="fa fa-download"></i>
                            Cetak Seluruh Absensi
                        </a>
                    @endif
                    <div class="box-body">
                        <h3>
                            Pegawai: {{$pegawai->user->nama}}
                        </h3>
                        
                        <div class="table-responsive">
                            <table id="tableAbsen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Jam Datang</th>
                                        <th>Jam Pulang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($absens as $absen)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{date('d-m-Y', strtotime($absen->tanggal))}}</td>
                                            <td>{{$absen->jam_datang}}</td>
                                            <td>{{$absen->jam_pulang}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableAbsen').dataTable()
        });
    </script>
@endsection
