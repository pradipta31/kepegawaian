@extends('layouts.master',['activeMenu' => 'absen'])
@section('title','Absensi')
@section('breadcrumb', 'Absensi Pegawai '. $pegawai->user->nama)
@section('detail_breadcrumb', 'Data Absensi')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    
                    <div class="box-body">
                        @if (Auth::user()->level == 3)
                        
                        @else
                            <div class="row">
                                <div class="col-xs-6">
                                    <form action="{{url('s')}}" method="GET" style="margin-right: 10px; margin-top: 10px">
                                        <input type="hidden" name="pegawai_id" value="{{$pegawai->id}}">
                                        <div class="form-group col-xs-5">
                                            <label for="">Pilih Tanggal</label>
                                            <input type="date" name="tanggal_awal" class="form-control">
                                        </div>
                                        <div class="form-group col-xs-5">
                                            <label for="">Pilih Tanggal</label>
                                            <input type="date" name="tanggal_akhir" class="form-control">
                                        </div>
                                        <div class="form-group col-xs-2">
                                            <button type="submit" class="btn btn-primary" style="margin-top: 25px">Submit</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-6">
                                    <a href="{{url('absenpegawai/'.$pegawai->id)}}" class="btn btn-primary btn-md pull-right" style="margin-top: 10px">
                                        <i class="fa fa-arrow-left"></i>
                                        Kembali
                                    </a>
                                </div>
                            </div>
                        @endif    
                        
                        <div class="table-responsive" style="margin-top: 10px">
                            <table id="tableAbsen" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Jam Datang</th>
                                        <th>Jam Pulang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableAbsen').dataTable()
        });
    </script>
@endsection
