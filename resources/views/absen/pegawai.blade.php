@extends('layouts.master',['activeMenu' => 'absen'])
@section('title','Absen Pegawai')
@section('breadcrumb', 'Absen Pegawai')
@section('detail_breadcrumb', 'Data Absen Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Bagian</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pegawais as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$user->user->nip}}
                                            </td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                {{$user->tempat->nama_tempat}}
                                            </td>
                                            <td>
                                                <a href="{{url('absenpegawai/'.$user->id)}}" class="btn btn-sm btn-primary">
                                                    <i class="fa fa-assistive-listening-systems"></i>
                                                    Lihat Absensi
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

    </script>
@endsection
