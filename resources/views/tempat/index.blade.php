@extends('layouts.master',['activeMenu' => 'tempat'])
@section('title','Sub Bagian')
@section('breadcrumb', 'Sub Bagian')
@section('detail_breadcrumb', 'Manajemen Data Sub Bagian')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('tempat/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Tambah Sub Bagian Baru
                        </a>
                        <div class="table-responsive">
                            <table id="tabelTempat" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Tempat</th>
                                        <th>Slot</th>
                                        <th>Jumlah</th>
                                        <th>Sisa</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tempats as $tempat)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$tempat->nama_tempat}}</td>
                                            <td>
                                                {{$tempat->slot}}
                                            </td>
                                            <td>{{$tempat->jumlah}}</td>
                                            <td>{{$tempat->sisa}}</td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#editTempat{{$tempat->id}}">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="deleteTempat('{{$tempat->id}}')">
                                                    <i class="fa fa-trash"></i>
                                                    Hapus
                                                </a>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="editTempat{{$tempat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">Edit Tempat : {{$tempat->nama_tempat}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{url('tempat/'.$tempat->id.'/edit')}}" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="put">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label">Nama Tempat</label>
                                                                        <input type="text" name="nama_tempat" class="form-control" value="{{$tempat->nama_tempat}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label">Slot</label>
                                                                        <input type="number" name="slot" class="form-control" value="{{$tempat->slot}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<form class="hidden" action="" method="post" id="formDelete">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="delete">
</form>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tabelTempat').dataTable()
        });

        function editTempatModal(){
            $('#editTempat').modal('show');
        }

        function deleteTempat(id){
            swal({
                title: "Anda yakin?",
                text: "Data sub bagian akan terhapus secara permanen!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    swal("Berhasil! Data sub bagian telah terhapus!", {
                        icon: "success",
                    }).then((res) => {
                        $('#formDelete').attr('action', '{{url('tempat/delete')}}/'+id);
                        $('#formDelete').submit();
                    }); 
                }
            });
        }
    </script>
@endsection
