@extends('layouts.master',['activeMenu' => 'tempat'])
@section('title','Tambah Sub Bagian')
@section('breadcrumb', 'Tambah Sub Bagian')
@section('detail_breadcrumb', 'Tambah Sub Bagian Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('tempat/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Sub Bagian Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Nama Sub Bagian</label>
                                <input type="text" class="form-control" name="nama_tempat" value="{{old('nama_tempat')}}" placeholder="Masukan Nama Sub Bagian">
                            </div>
                            <div class="form-group">
                                <label for="">Slot</label>
                                <input type="number" name="slot" class="form-control" value="{{old('slot')}}" placeholder="Masukkan jumlah maksimal pegawai">
                            </div>
                            <div class="box-footer">
                                <a href="{{url('tempat')}}" class="btn btn-default">Kembali</a>
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection