@extends('layouts.master',['activeMenu' => 'pegawai'])
@section('title','Golongan dan Pangkat')
@section('breadcrumb', 'Golongan dan Pangkat')
@section('detail_breadcrumb', 'Data Golongan dan Pangkat')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <h3>
                            Bagian: {{$tempat->nama_tempat}}
                        </h3>
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Golongan/Pangkat</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pegawais as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$user->user->nip}}
                                            </td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                @if ($user->golongan == 1)
                                                    GOLONGAN I (Juru)/{{$user->pangkat}}
                                                @elseif($user->golongan == 2)
                                                    GOLONGAN II (Pengatur)/{{$user->pangkat}}
                                                @elseif($user->golongan == 3)
                                                    GOLONGAN III (Penata)/{{$user->pangkat}}
                                                @elseif($user->golongan == 4)
                                                    GOLONGAN IV (Pembina)/{{$user->pangkat}}
                                                @endif
                                            </td>
                                            <td>
                                                @if (Auth::user()->level == 3)
                                                    -    
                                                @else
                                                    <a href="{{url('pegawai/'.$user->user_id.'/golkat')}}" class="btn btn-sm btn-warning">
                                                        <i class="fa fa-pencil"></i>
                                                        Edit Golongan/Pangkat
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

    </script>
@endsection
