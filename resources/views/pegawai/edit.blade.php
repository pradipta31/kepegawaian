@extends('layouts.master',['activeMenu' => 'pegawai'])
@section('title','Perbaharui Data Pegawai')
@section('breadcrumb', 'Perbaharui Data Pegawai')
@section('detail_breadcrumb', 'Perbaharui Data Pegawai')
@section('content')
    @include('layouts.breadcrumb')
    <style>
        select option[disabled] {
            display: none;
        }
    </style>
    <section class="content">
        <form class="" action="{{url('pegawai/golkat')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="user_id" value="{{$pegawai->user_id}}">
            <input type="hidden" name="tempat_id" value="{{$pegawai->tempat_id}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4>Bagian : <b>{{$pegawai->tempat->nama_tempat}}</b></h4>
                            <h4>Nama : <b>{{$pegawai->user->nama}}</b></h4>
                            <h4>NIP : <b>{{$pegawai->user->nip}}</b></h4>
                            <h4>
                                Golongan/Pangkat Sebelumnya: 
                                @if ($pegawai->golongan == 1)
                                    <b>GOLONGAN I (Juru)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 2)
                                    <b>GOLONGAN II (Pengatur)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 3)
                                    <b>GOLONGAN III (Penata)/{{$pegawai->pangkat}}</b>
                                @elseif($pegawai->golongan == 4)
                                    <b>GOLONGAN IV (Pembina)/{{$pegawai->pangkat}}</b>
                                @endif
                            </h4>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Pilih Golongan</label>
                                <select name="golongan" class="form-control" value="{{old('golongan')}}" id="golongan_id">
                                    <option value="">Pilih Golongan</option>
                                    <option value="1">GOLONGAN I (Juru)</option>
                                    <option value="2">GOLONGAN II (Pengatur)</option>
                                    <option value="3">GOLONGAN III (Penata)</option>
                                    <option value="4">GOLONGAN IV (Pembina)</option>
                                </select>
                            </div>

                            @include('user.pangkat')
                            <div class="box-footer">
                                <a href="{{url('pegawai/'.$pegawai->tempat_id.'/datapegawai')}}" class="btn btn-default">Kembali</a>
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
    <script type="text/javascript">

        $('#pangkat_1').hide();
        $('#pangkat_2').hide();
        $('#pangkat_3').hide();
        $('#pangkat_4').hide();

        $('#golongan_id').on('change',function(){
            var valueSelected = this.value;
            
            if(valueSelected == 1){
                $('#pangkat_1').show();
                $('#golongan_1').show();
                $('#golongan_1').prop('disabled', false);
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');

                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();
            }else if(valueSelected == 2){
                $('#pangkat_1').hide();
                $('#pangkat_2').show();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();

                $('#golongan_2').show();
                $('#golongan_2').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }else if(valueSelected == 3){
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').show();
                $('#pangkat_4').hide();
                
                $('#golongan_3').show();
                $('#golongan_3').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }else if(valueSelected == 4){
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').show();
                
                $('#golongan_4').show();
                $('#golongan_4').prop('disabled', false);
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
            }else{
                $('#pangkat_1').hide();
                $('#pangkat_2').hide();
                $('#pangkat_3').hide();
                $('#pangkat_4').hide();
                $('#golongan_1').prop('disabled', 'disabled');
                $('#golongan_2').prop('disabled', 'disabled');
                $('#golongan_3').prop('disabled', 'disabled');
                $('#golongan_4').prop('disabled', 'disabled');
            }
        });

        function saveThis(r){
            swal({
                title: "User baru akan ditambahkan",
                text: "User akan menerima email dan harus melakukan konfirmasi untuk dapat melakukan login.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((result) => {
                if (result) {
                    swal("Email berhasil terkirim!", {
                        icon: "success",
                    });
                }
            });
        }
    </script>
@endsection