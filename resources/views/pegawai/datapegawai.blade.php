@extends('layouts.master',['activeMenu' => 'pegawai'])
@section('title','Pegawai')
@section('breadcrumb', 'Pegawai')
@section('detail_breadcrumb', 'Data Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Bagian</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Golongan/Pangkat</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$user->tempat->nama_tempat}}</td>
                                            <td>
                                                {{$user->user->nip}}
                                            </td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>{{$user->user->email}}</td>
                                            <td>
                                                @if ($user->golongan == 1)
                                                    GOLONGAN I (Juru)/{{$user->pangkat}}
                                                @elseif($user->golongan == 2)
                                                    GOLONGAN II (Pengatur)/{{$user->pangkat}}
                                                @elseif($user->golongan == 3)
                                                    GOLONGAN III (Penata)/{{$user->pangkat}}
                                                @elseif($user->golongan == 4)
                                                    GOLONGAN IV (Pembina)/{{$user->pangkat}}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($user->user->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function editUserModal(){
            $('#editUser').modal('show');
        }
    </script>
@endsection
