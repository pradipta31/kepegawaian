@extends('layouts.master',['activeMenu' => 'mutasi'])
@section('title','Mutasi Pegawai')
@section('breadcrumb', 'Mutasi Pegawai')
@section('detail_breadcrumb', 'Mutasi Data Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        @if (Auth::user()->level == 3)
                            
                        @else
                            <a href="{{url('mutasi')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                                <i class="fa fa-exchange"></i>
                                Mutasi Pegawai
                            </a>
                        @endif
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Bagian Sebelumnya</th>
                                        <th>Bagian Sekarang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mutasis as $mutasi)
                                    @php
                                        $tempat_lama = \App\Tempat::where('id', $mutasi->tempat_lama)->first();
                                        $tempat_baru = \App\Tempat::where('id', $mutasi->tempat_baru)->first();
                                    @endphp
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$mutasi->pegawai->user->nip}}
                                            </td>
                                            <td>{{$mutasi->pegawai->user->nama}}</td>
                                            <td>
                                                {{$tempat_lama->nama_tempat}}
                                            </td>
                                            <td>
                                                {{$tempat_baru->nama_tempat}}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function mutasiPegawaiModal(){
            $('#mutasiPegawai').modal('show')
        }
    </script>
@endsection
