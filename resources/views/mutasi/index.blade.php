@extends('layouts.master',['activeMenu' => 'mutasi'])
@section('title','Mutasi Pegawai')
@section('breadcrumb', 'Mutasi Pegawai')
@section('detail_breadcrumb', 'Mutasi Data Pegawai')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <h3>
                            Bagian: {{$tempat->nama_tempat}}
                        </h3>
                        <div class="table-responsive">
                            <table id="tableUser" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="10px">No</th>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Golongan/Pangkat</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pegawais as $user)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>
                                                {{$user->user->nip}}
                                            </td>
                                            <td>{{$user->user->nama}}</td>
                                            <td>
                                                @if ($user->golongan == 1)
                                                    GOLONGAN I (Juru)/{{$user->pangkat}}
                                                @elseif($user->golongan == 2)
                                                    GOLONGAN II (Pengatur)/{{$user->pangkat}}
                                                @elseif($user->golongan == 3)
                                                    GOLONGAN III (Penata)/{{$user->pangkat}}
                                                @elseif($user->golongan == 4)
                                                    GOLONGAN IV (Pembina)/{{$user->pangkat}}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-info" data-toggle="modal" data-target="#mutasiPegawai{{$user->user_id}}">
                                                    <i class="fa fa-exchange"></i>
                                                    Mutasi
                                                </a>
                                            </td>
                                        </tr>
                                        <div class="modal fade" id="mutasiPegawai{{$user->user_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h3 class="modal-title" id="exampleModalLabel">Mutasi Pegawai : {{$user->user->nama}}</h3>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{url('mutasi/mutasi-pegawai')}}" method="post" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="user_id" value="{{$user->user_id}}">
                                                        <input type="hidden" name="tempat_lama" value="{{$tempat->id}}">
                                                        <div class="modal-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label">Tempat Sebelumnya</label>
                                                                        <input type="text" name="nama_tempat" class="form-control" value="{{$tempat->nama_tempat}}" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label">Mutasi Ke Bagian</label>
                                                                        <select name="tempat_id" class="form-control" value="{{old('tempat_id')}}">
                                                                            @foreach ($tempats as $item)
                                                                                <option value="{{$item->id}}">{{$item->nama_tempat}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label">Keterangan</label>
                                                                        <textarea name="keterangan" class="form-control" cols="30" rows="5">{{old('keterangan')}}</textarea>
                                                                        <small>Keterangan dapat dikosongkan.</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <small>
                            <div class="row">
                                <div class="col-md-4">
                                    Total Pegawai: {{$tempat->jumlah}} Orang
                                </div>
                                <div class="col-md-4">
                                    Jumlah Maks Pegawai: {{$tempat->slot}} Orang
                                </div>
                                <div class="col-md-4">
                                    Jumlah Sisa Tempat: {{$tempat->sisa}} Orang
                                </div>
                            </div>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tableUser').dataTable()
        });

        function mutasiPegawaiModal(){
            $('#mutasiPegawai').modal('show')
        }
    </script>
@endsection
