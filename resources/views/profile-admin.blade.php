@extends('layouts.master',['activeMenu' => ''])
@section('title', 'Profile Saya')
@section('breadcrumb', 'Profile Saya')
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <div class="text-center">
                            @if ($user->avatar == null)
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('images/ava.png')}}"
                                    alt="User profile picture">
                            @else
                                <img class="profile-user-img img-fluid img-circle"
                                    src="{{asset('images/ava/'.$user->avatar)}}"
                                    alt="User profile picture">
                            @endif
                        </div>
    
                        <h3 class="profile-username text-center">{{$user->nip}}</h3>
                        <h3 class="profile-username text-center">{{$user->nama}}</h3>
                        <p class="text-muted text-center">
                            @if ($user->level == 3)
                                Pegawai
                            @else
                                Admin
                            @endif
                        </p>
    
                        <ul class="list-group list-group-unbordered mb-3">
                            
                            <li class="list-group-item">
                                <b>Username</b> <a class="pull-right">{{$user->username}}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Email</b> <a class="pull-right">{{$user->email}}</a>
                            </li>
                        </ul>
    
                        <button class="btn btn-primary btn-block" id="editProfile">
                            <i class="fa fa-pencil"></i>
                            Edit Profile
                        </button>
                        <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#editAva{{$user->id}}">
                            <i class="fa fa-pencil-square-o"></i>
                            Edit Foto
                        </button>

                        <div class="modal fade" id="editAva{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Ubah Foto Anda</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="{{url('profile/avatar/'.$user->id.'/edit')}}" method="post" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="put">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Masukan Foto Anda</label>
                                                        <input type="file" class="form-control" name="avatar">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for=""></label>
                                                        @if ($user->ava == null)
                                                            <span>Foto tidak ditemukan.</span>
                                                        @else
                                                            <a href="{{asset('images/ava/'.$user->ava)}}" target="_blank">
                                                                <img class="profile-user-img img-fluid"
                                                                    src="{{asset('images/ava/'.$user->ava)}}"
                                                                    alt="User profile picture">
                                                            </a>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                                            <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan & Kirim</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="box box-primary" id="formProfile" style="display: none">
                    <div class="box-body">
                        <form action="{{url('profile/bio/'.$user->id.'/edit')}}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="_method" value="put">
                            {{-- <div class="form-group row">
                                <label class="col-sm-2 col-form-label">NIP</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nip" class="form-control" placeholder="Masukan NIP (Nomor Induk Pegawai)" value="{{$user->nip}}" disabled>
                                </div>
                            </div> --}}
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Username</label>
                                <div class="col-sm-10">
                                    <input type="text" name="username" class="form-control" placeholder="Masukan username" value="{{$user->username}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10">
                                    <input type="text" name="nama" class="form-control" placeholder="Masukan nama" value="{{$user->nama}}">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" placeholder="Masukan email" value="{{$user->email}}">
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Masukan password">
                                    <small>Kosongkan jika tidak ingin mengubah password</small>
                                </div>
                                
                            </div>
                            
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Re-type Password</label>
                                <div class="col-sm-10">
                                    <input type="password" name="confirmation_password" id="confirmation_password" class="form-control" placeholder="Masukan password ulang">
                                    <span id="message"></span>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="offset-sm-2 col-sm-10">
                                    <button type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i>
                                        Simpan Perubahan
                                    </button>
                                </div>
                            </div>
                        </form>
                            
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script type="text/javascript">
        function editAvaModal(){
            $('#editAva').modal('show')
        }
        $(document).ready(function(){
            $('#editProfile').click( function() {
                $('#formProfile').toggle('slow');
            });
        });

        $('#password, #confirmation_password').on('keyup', function () {
            if ($('#password').val() == $('#confirmation_password').val()) {
                $('#message').html('Password dapat digunakan!').css('color', 'green');
            } else {
                $('#message').html('Password tidak sama!').css('color', 'red');
            }
        });
    </script>
@endsection