@extends('layouts.master',['activeMenu' => 'pengumuman'])
@section('title','Edit Pengumuman')
@section('breadcrumb', 'Edit Pengumuman')
@section('detail_breadcrumb', 'Edit Pengumuman')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('pengumuman/'.$pengumuman->id.'/edit')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Edit Pengumuman</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tanggal Pengumuman</label>
                                <input type="date" name="tanggal" class="form-control" value="{{$pengumuman->tanggal}}" style="height:25%">
                            </div>
                            <div class="form-group">
                                <label for="">Judul Pengumuman</label>
                                <input type="text" name="judul" class="form-control" value="{{$pengumuman->judul}}" placeholder="Masukan judul">
                            </div>
                            <div class="form-group">
                                <label for="">Isi Pengumuman</label>
                                <textarea name="isi" id="" cols="30" rows="5" class="form-control">{{$pengumuman->isi}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                                @if ($pengumuman->gambar == null)
                                    No results found.
                                @else
                                    <img src="{{asset('images/pengumuman/'.$pengumuman->gambar)}}" alt="" class="img-responsive">
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="">Tanda Tangan</label>
                                <input type="text" name="ttd" class="form-control" value="{{$pengumuman->ttd}}" placeholder="Masukkan Tanda Tangan">
                            </div>

                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{$pengumuman->status}}">
                                    <option value="1" {{$pengumuman->status == 1 ? 'selected' : ''}}>Aktif</option>
                                    <option value="0" {{$pengumuman->status == 0 ? 'selected' : ''}}>Non Aktif</option>
                                </select>
                                <small>Note: status aktif maka akan langsung ditampilkan.</small>
                            </div>
                            
                            <div class="box-footer">
                                <a href="{{url('pengumuman')}}" class="btn btn-default">Kembali</a>
                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    });
</script>
@endsection