@extends('layouts.master',['activeMenu' => 'pengumuman'])
@section('title','Pengumuman')
@section('breadcrumb', 'Pengumuman')
@section('detail_breadcrumb', 'Manajemen Data Pengumuman')
@section('css')
    <link rel="stylesheet" href="{{asset('backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/bower_components/select2/dist/css/select2.min.css')}}">
@endsection
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <a href="{{url('pengumuman/tambah')}}" class="btn btn-md btn-primary" style="margin-bottom: 10px">
                            <i class="fa fa-plus"></i>
                            Buat Pengumuman Baru
                        </a>
                        <div class="table-responsive">
                            <table id="tablePengumuman" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Judul</th>
                                        <th>Isi</th>
                                        <th>Gambar</th>
                                        <th>TTD</th>
                                        <th>Status</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pengumumans as $pengumuman)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{date('d-m-Y', strtotime($pengumuman->tanggal))}}</td>
                                            <td>{{$pengumuman->judul}}</td>
                                            <td>{!! $pengumuman->isi !!}</td>
                                            <td>
                                                @if ($pengumuman->gambar == null)
                                                    -
                                                @else
                                                    <img src="{{asset('images/pengumuman/'.$pengumuman->gambar)}}" class="img-responsive">
                                                @endif
                                            </td>
                                            <td>{{$pengumuman->ttd}}</td>
                                            <td>
                                                @if ($pengumuman->status == 1)
                                                    <span class="label label-success">Aktif</span>
                                                @else
                                                    <span class="label label-warning">Non Aktif</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{url('pengumuman/'.$pengumuman->id.'/edit')}}" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-pencil"></i>
                                                    Edit
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{asset('backend/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $(function(){
            $('#tablePengumuman').dataTable()
        });
    </script>
@endsection
