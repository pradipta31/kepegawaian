@extends('layouts.master',['activeMenu' => 'pengumuman'])
@section('title','Tambah Pengumuman')
@section('breadcrumb', 'Tambah Pengumuman')
@section('detail_breadcrumb', 'Tambah Pengumuman Baru')
@section('content')
    @include('layouts.breadcrumb')
    <section class="content">
        <form class="" action="{{url('pengumuman/tambah')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Tambah Pengumuman Baru</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="">Tanggal Pengumuman</label>
                                <input type="date" name="tanggal" class="form-control" value="{{old('tanggal')}}" style="height:25%">
                            </div>
                            <div class="form-group">
                                <label for="">Judul Pengumuman</label>
                                <input type="text" name="judul" class="form-control" value="{{old('judul')}}" placeholder="Masukan judul">
                            </div>
                            <div class="form-group">
                                <label for="">Isi Pengumuman</label>
                                <textarea name="isi" id="" cols="30" rows="5" class="form-control">{{old('isi')}}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="">Gambar</label>
                                <input type="file" name="gambar" class="form-control">
                                <small>Gambar dapat dikosongkan.</small>
                            </div>
                            <div class="form-group">
                                <label for="">Tanda Tangan</label>
                                <input type="text" name="ttd" class="form-control" value="{{old('ttd')}}" placeholder="Masukkan Tanda Tangan">
                            </div>

                            <div class="form-group">
                                <label for="">Status</label>
                                <select name="status" class="form-control" value="{{old('status')}}">
                                    <option value="1">Aktif</option>
                                    <option value="0">Non Aktif</option>
                                </select>
                                <small>Note: status aktif maka akan langsung ditampilkan.</small>
                            </div>
                            
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary" onclick="saveBtn(this)">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection
@section('js')
<script src="{{asset('backend/plugins/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('backend/plugins/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        resize: false,
        plugins: [
            ' advlist anchor autolink codesample fullscreen help',
            ' lists link media noneditable preview',
            ' searchreplace table visualblocks wordcount'
        ],

        toolbar:
        'insertfile undo redo | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist',
    });

    function saveThis(r){
        swal({
            title: "User baru akan ditambahkan",
            text: "User akan menerima email dan harus melakukan konfirmasi untuk dapat melakukan login.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((result) => {
            if (result) {
                swal("Email berhasil terkirim!", {
                    icon: "success",
                });
            }
        });
    }
</script>
@endsection