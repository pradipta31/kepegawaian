<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nip' => '1234567890',
                'nama' => 'Kasubag',
                'username' => 'kasubag',
                'email' => 'kasubag@gmail.com',
                'password' => Hash::make('123456'),
                'avatar' => null,
                'level' => 1,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
            [
                'nip' => null,
                'nama' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('123456'),
                'avatar' => null,
                'level' => 2,
                'status' => 1,
                'created_at' => NOW(),
                'updated_at' => NOW()
            ],
        ]);
    }
}
