<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('iloveuasri', function(){
    return view('coba.index');
});
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['register' => false, 'verify' => true]);

Route::get('/admin', function(){
    return redirect('/home');
});

Route::get('attendance', 'AbsenController@attendance');
Route::post('att', 'AbsenController@absen');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('tempat', 'TempatController@index');
    Route::get('tempat/tambah', 'TempatController@create');
    Route::post('tempat/tambah', 'TempatController@store');
    Route::put('tempat/{id}/edit', 'TempatController@update');
    Route::delete('tempat/delete/{id}', 'TempatController@destroy');

    Route::get('user/tambah', 'UserController@create');
    Route::post('user/tambah', 'UserController@store');
    Route::get('user', 'UserController@index');
    Route::get('user/{id}/edit', 'UserController@edit');
    Route::put('user/{id}/edit', 'UserController@update');
    Route::get('user/qrcode/{id}', 'UserController@qrCode');

    Route::get('pegawai', 'PegawaiController@tempat');
    Route::get('pegawai/{id}/datapegawai', 'PegawaiController@allPegawai');
    Route::get('pegawai/{id}/golkat', 'PegawaiController@editGolkat');
    Route::post('pegawai/golkat', 'PegawaiController@updateGolkat');

    Route::get('pegawai/all', 'PegawaiController@dataPegawai');

    Route::get('absen', 'AbsenController@index');
    Route::get('absenpegawai/{id}', 'AbsenController@absenPegawai');
    Route::get('absen/cetak/{pegawai_id}', 'AbsenController@cetakAbsen');
    Route::get('absenme', 'AbsenController@absenMe');
    Route::get('absen/cari/{id}', 'AbsenController@cariAbsen');
    Route::get('s', 'AbsenController@searchAbsen');
    Route::get('cetak/{tanggal_awal}/{tanggal_akhir}/{pegawai_id}', 'AbsenController@cetakkkk');
    

    Route::get('pengumuman/tambah', 'PengumumanController@create');
    Route::post('pengumuman/tambah', 'PengumumanController@store');
    Route::get('pengumuman', 'PengumumanController@index');
    Route::get('pengumuman/{id}/edit', 'PengumumanController@edit');
    Route::put('pengumuman/{id}/edit', 'PengumumanController@update');
    

    Route::get('datamutasi', 'MutasiController@mutasi');
    Route::get('mutasi', 'MutasiController@tempat');
    Route::get('mutasi/{id}/pegawai', 'MutasiController@allPegawai');
    Route::post('mutasi/mutasi-pegawai', 'MutasiController@mutasiPegawai');

    Route::get('datacuti', 'CutiController@cuti');
    Route::get('cuti', 'CutiController@allPegawai');
    Route::get('cuti/tambah/{id}', 'CutiController@tambahCuti');
    Route::get('cuti/tambah', 'CutiController@addCuti');
    Route::post('cuti/tambah', 'CutiController@store');
    Route::get('cuti/download/{id}', 'CutiController@download')->name('cuti.download');
    Route::get('pegawai/cuti', 'CutiController@index');
    Route::get('datacuti/{id}/edit', 'CutiController@editCuti');
    Route::put('datacuti/{id}/edit', 'CutiController@UpdateCuti');
    Route::put('pegawai/cuti/setuju/{id}', 'CutiController@setujuCuti');
    Route::put('pegawai/cuti/tolak/{id}', 'CutiController@tolakCuti');

    Route::get('profile', 'ProfileController@index');
    Route::put('profile/bio/{id}/edit', 'ProfileController@updateProfile');
    Route::put('profile/avatar/{id}/edit', 'ProfileController@updateAva');
    Route::put('profile/password/{id}', 'ProfileController@updatePassword');
});
